# KREA SITEBUILD #
2017


### Fejlesztő környezet ###
[https://atom.io/](https://atom.io/)

### less-autocompile ###
Installálás az Atom-ban: File -> Settings -> Install, keresőmezőbe beírod, hogy: less-autocompile, majd mellette megnyomod a Packages gombot, ha megtalálta akkor installálod.

Itt pedig le van irva, hogy hogyan kell konfigurálni ;)
[https://atom.io/packages/less-autocompile](https://atom.io/packages/less-autocompile)


## TERVEK ##

### Alapok ###
[https://www.figma.com/file/SpBw4k4qj2duX9bCfwquymQV/Alapok](https://www.figma.com/file/SpBw4k4qj2duX9bCfwquymQV/Alapok)

### Kezdő ###
[https://www.figma.com/file/Clgqo6Q600CwlpbMNHpGoVKk/Kezdo](https://www.figma.com/file/Clgqo6Q600CwlpbMNHpGoVKk/Kezdo)

### Haladó ###
[https://www.figma.com/file/PxYSvUIQ9NXlkuI1De9Soljc/Halado](https://www.figma.com/file/PxYSvUIQ9NXlkuI1De9Soljc/Halado)

# VIZSGAFELADAT LEADÁSA #

Könyvtár struktúra:

```
#!python

[vezetkenev-keresztnev]
|___design.jpg (a megvalósított anyag eredeti dizájnterve)
|___[source]
    |___[img]
    |___index.html
    |___style.css
```

Elérhetőség:
[attila@abg.hu](mailto:attila@abg.hu)
Lécci jelöljétek hogy melyik csoportból írtok pl.: KREA - Grafikus | blablabla....